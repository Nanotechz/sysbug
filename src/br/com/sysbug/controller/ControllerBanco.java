package br.com.sysbug.controller;

import java.util.ArrayList;

import br.com.sysbug.model.Banco;
import br.com.sysbug.model.Cliente;
import br.com.sysbug.model.Conta;
import br.com.sysbug.model.Funcionario;

public class ControllerBanco {
	private Banco banco;

	public ControllerBanco(Banco banco) {
		this.banco = banco;
	}
	
	public void adicionarConta(Conta conta){
		ArrayList<Conta> contas = this.banco.getContas();
		
		if(!contas.contains(conta)){
			contas.add(conta);
		}
	}
	
	public Conta pegarConta(int x){
		ArrayList<Conta> contas = this.banco.getContas();
		
		for(Conta conta : contas){
			if (conta.getCodigo() == x)
				return conta;
		}
		
		return null;
	}
	
	public int pegarTotalDeContas(){
		ArrayList<Conta> contas = this.banco.getContas();
		
		return contas.size();
	}
	
	public void excluirConta(Conta conta){
		ArrayList<Conta> contas = this.banco.getContas();
		
		contas.remove(conta);
	}

	public void adicionarFuncionario(Funcionario funcionario){
		ArrayList<Funcionario> funcionarios = this.banco.getFuncionarios();
		
		funcionarios.add(funcionario);
	}
	
	public void excluirFuncionario(Funcionario funcionario){
		ArrayList<Funcionario> funcionarios = this.banco.getFuncionarios();
		
		funcionarios.remove(funcionario);
	}
	
	public void adicionarCliente(Cliente cliente){
		ArrayList<Cliente> clientes = this.banco.getClientes();
		
		clientes.add(cliente);
	}
	
	public void removerCliente(Cliente cliente){
		ArrayList<Cliente> clientes = this.banco.getClientes();
		
		clientes.remove(cliente);
	}	
}
