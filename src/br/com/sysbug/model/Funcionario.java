package br.com.sysbug.model;

import java.util.ArrayList;

public class Funcionario {
	private String nome;
	private String cpf;
	private String codigo;
	private double salario;
	private ArrayList<String> telefones;
	private Endereco endereco;
	
	public Funcionario(String nome, String cpf){
		this.nome = nome;
		this.cpf = cpf;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public ArrayList<String> getTelefones() {
		return telefones;
	}
	
	public void setTelefones(ArrayList<String> telefones) {
		this.telefones = telefones;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}
}
