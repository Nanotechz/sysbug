package br.com.sysbug.model;

public abstract class Conta {
	private double saldo;
	private int codigo;
	private Cliente cliente;
	

	public Conta(Cliente cliente, double saldo, int codigo) {
		this.cliente = cliente;
		this.saldo = saldo;
		this.codigo = codigo;
	}
	
	protected void setSaldo(double valor){
		this.saldo = valor;
	}
	
	public double getSaldo() {
		return saldo;
	}

	public int getCodigo() {
		return codigo;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public abstract boolean sacar(double valor);
	
	public void depositar(double valor){
		if(valor > 0){
			this.saldo += valor;
		}
	}
	
	public void extrato(){
		System.out.println("Saldo= " + this.saldo);
	}
	
	public abstract void atualizar(double taxaSelic);
	
	
}
