package br.com.sysbug.model;

public class Gerente extends Funcionario{

	private String senha;
	private int numeroDeFuncionariosGerenciados;
	
	public Gerente(String nome, String cpf) {
		super(nome, cpf);
		// TODO Auto-generated constructor stub
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public int getNumeroPessoas() {
		return numeroDeFuncionariosGerenciados;
	}

	public void setNumeroPessoas(int numeroPessoas) {
		this.numeroDeFuncionariosGerenciados = numeroPessoas;
	}
	
	
}
