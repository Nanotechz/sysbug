package br.com.sysbug.model;

import java.util.ArrayList;

public class Cliente {
	private String nome;
	private ArrayList<String> telefone;
	private Endereco endereco;
	
	public Cliente(String nome){
		this.nome = nome;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public ArrayList<String> getTelefone() {
		return telefone;
	}
	public void setTelefone(ArrayList<String> telefone) {
		this.telefone = telefone;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	
}
