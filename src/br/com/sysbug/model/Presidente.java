package br.com.sysbug.model;

public class Presidente extends Funcionario{
	private String senha;
	
	public Presidente(String nome, String cpf) {
		super(nome, cpf);
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

}
