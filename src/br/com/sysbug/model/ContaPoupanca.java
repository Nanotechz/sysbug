package br.com.sysbug.model;

public class ContaPoupanca extends Conta{

	public ContaPoupanca(Cliente cliente, double saldo, int codigo) {
		super(cliente, saldo, codigo);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean sacar(double valor) {
		if(valor <=  super.getSaldo() && valor > 0){
			valor = - valor + super.getSaldo();
			super.setSaldo(valor);
			return true;
		}
		
		return false;
	}

	@Override
	public void atualizar(double taxaSelic) {
			super.setSaldo(super.getSaldo() + super.getSaldo() * taxaSelic * 3);
			
	}	
}
