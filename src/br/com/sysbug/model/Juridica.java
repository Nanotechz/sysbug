package br.com.sysbug.model;

public class Juridica extends Cliente{
	private String razaoSocial;
	private String cnpj;
	
	public Juridica(String nome, String cnpj) {
		super(nome);
		this.cnpj = cnpj;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	
	

}
