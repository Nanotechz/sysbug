package br.com.sysbug.model;

public class ContaCorrente extends Conta {
	private double credito;
	
	public ContaCorrente(Cliente cliente, double saldo, int codigo, double credito) {
		super(cliente, saldo, codigo);
		this.credito = credito;
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean sacar(double valor) {
		if(valor <= 0){
			return false;
		}
		
		if(valor <= (super.getSaldo() + credito)){
			valor = - valor + super.getSaldo();
			super.setSaldo(valor);
			return true;
		}
		
		return false;
	}
	
	@Override
	public void atualizar(double taxaSelic) {
			super.setSaldo(super.getSaldo() + super.getSaldo() * taxaSelic *2);
			
	}
}
