package br.com.sysbug.model;

public class Fisica extends Cliente{
	private String cpf;

	public Fisica(String nome, String cpf){
		super(nome);
		this.cpf = cpf;
	}
	
	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
}
