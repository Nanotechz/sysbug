package br.com.sysbug.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.sysbug.controller.ControllerBanco;
import br.com.sysbug.model.Banco;
import br.com.sysbug.model.Cliente;
import br.com.sysbug.model.Conta;
import br.com.sysbug.model.ContaPoupanca;
import br.com.sysbug.model.Funcionario;

public class TestControllerBanco {
	private Banco banco;
	private ControllerBanco controller;
	private Conta contaTest;
	private Funcionario funcionarioTest;
	private Cliente clienteTest;
	
	@Before
	public void inicializar(){		
		this.banco = new Banco("123456789", "Banco FGA");
		this.controller = new ControllerBanco(banco);
		this.clienteTest = new Cliente("asdj");
		this.contaTest = new ContaPoupanca(clienteTest, 0, 45);
		this.funcionarioTest = new Funcionario("iooi", "154");
		
	}
	
	
	@Test
	public void testAddConta() {
		controller.adicionarConta(contaTest);
		assertEquals(1, banco.getContas().size());
	}
	
	@Test
	public void testPegarNumeroDeContas(){
		controller.adicionarConta(contaTest);
		assertEquals(1, controller.pegarTotalDeContas());
	}
	
	@Test
	public void testRemoverConta(){
		controller.excluirConta(contaTest);
		assertEquals(0, banco.getContas().size());
	}
	
	@Test
	public void testAdicionarFuncionario(){
		controller.adicionarFuncionario(funcionarioTest);
		assertEquals(1, banco.getFuncionarios().size());
	}
	
	@Test
	public void testRemoverFuncionario(){
		controller.excluirFuncionario(funcionarioTest);
		assertEquals(0, banco.getFuncionarios().size());
	}
	
	@Test
	public void testAdicionarCliente(){
		controller.adicionarCliente(clienteTest);
		assertEquals(1, banco.getClientes().size());
	}
	
	@Test
	public void testRemoverCliente(){
		controller.removerCliente(clienteTest);
		assertEquals(0, banco.getClientes().size());
	}
	
	@Test
	public void testPegarConta(){
		controller.adicionarConta(contaTest);
		
		assertNotNull(controller.pegarConta(45));
	}

}
