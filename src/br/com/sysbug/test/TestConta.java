package br.com.sysbug.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.sysbug.model.Cliente;
import br.com.sysbug.model.Conta;
import br.com.sysbug.model.ContaPoupanca;

public class TestConta {
	private Conta conta;
	private Cliente testCliente;
	private final double MARGEM_ERRO = 0.00001;
	
	@Before
	public void inicializar(){
		testCliente = new Cliente("Fulano");
		conta = new ContaPoupanca(testCliente, 5000.0, 123);
	}
	
	@Test
	public void testDepositar() {
		conta.depositar(500.0);
		
		assertEquals(5500.0, conta.getSaldo(), MARGEM_ERRO);
	}
	
	@Test
	public void testDepositarValorNegativo() {
		conta.depositar(-500.0);
		
		assertEquals(5000.0, conta.getSaldo(), MARGEM_ERRO);
	}

}
