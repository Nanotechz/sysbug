package br.com.sysbug.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import br.com.sysbug.model.Cliente;
import br.com.sysbug.model.Conta;
import br.com.sysbug.model.ContaCorrente;

public class TestContaCorrente {

	private Conta conta;
	private Cliente testCliente;
	private final double MARGEM_ERRO = 0.00001;
	
	@Before
	public void inicializar(){
		testCliente = new Cliente("Fulano");
	}
	
	@Test
	public void testSacar() {
		conta = new ContaCorrente(testCliente, 100.0, 123, 1000.0);
		
		assertEquals(true, conta.sacar(15));
		assertEquals(85.0, conta.getSaldo(), MARGEM_ERRO);
	}
	
	@Test
	public void testSacarValorNegativo() {
		conta = new ContaCorrente(testCliente, 100.0, 123, 1000.0);
		
		assertEquals(false, conta.sacar(-10));	
		assertEquals(100.0, conta.getSaldo(), MARGEM_ERRO);
	}
	
	@Test
	public void testSacarAcimaSaldo() {
		conta = new ContaCorrente(testCliente, 100.0, 123, 1000.0);
		
		assertEquals(true, conta.sacar(150));	
		assertEquals(-50.0, conta.getSaldo(), MARGEM_ERRO);
	}

}
